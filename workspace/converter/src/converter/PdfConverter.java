// class handling conversion to pdf 
package converter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfConverter extends Converter  // inherits from Converter
{
	public void setExten(String exten) 
	{
		this.exten = exten;
	}
	public void convert()
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Document document = new Document();
		PdfWriter writer = null;
		try 
		{
			writer = PdfWriter.getInstance(document, new FileOutputStream("movie.pdf"));
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (DocumentException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();
	
		for(int i=0;i<iden.size();i++)
		{
			// display the field
			System.out.print(iden.get(i));
			
			// read field value
			String lineFromInput = null;
			try 
			{
				lineFromInput = in.readLine();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// write to file
			try 
			{
				document.add(new Paragraph(iden.get(i) + lineFromInput));
			} 
			catch (DocumentException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		document.close();
		writer.close();
	}
}
