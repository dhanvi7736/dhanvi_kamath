// main program which calls the converter
package converter;

import java.io.*;
import java.util.*;
public class ConvertCaller
{
	public static void main(String[] args) 
	{
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the extension : ");
		String exten = null;
		try 
		{
			exten = in.readLine();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// to convert to pdf, the convert function is called with an object of PdfConverter
		if(Objects.equals(exten,"pdf"))
		{
			PdfConverter pdfConverter = new PdfConverter(); 
			pdfConverter.setExten(exten);
			pdfConverter.convert();
		}
		
		// to convert to other formats, the convert function is called with an object of OtherConverter
		else if(!Objects.equals(exten,""))
		{
				OtherConverter otherConverter = new OtherConverter();
				otherConverter.setExten(exten);
				otherConverter.convert();
		}
		
		// if no extension is entered
		else
		{
        		System.out.println("No extension specified. Program terminated");
   		}
	}
}
